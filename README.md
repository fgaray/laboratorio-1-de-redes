# Compilación


opp_makemake


# Descripción del enunciado


- Se tienen dos máquinas que deben conectarse.
- Hay una serie de indicaciones con las cuales debemos probar nuestras
  simulaciones y ver que tal se comportan.
- Las estaciones son de 3 capas emulando el estándar OSI: Una capa de
  aplicación, enlace y una intermedia que hace el trabajo de las capas que se
  encuentran en el medio.


## Comunicación

- Se inicia la simulación
- Se conecta con la otra máquina según HDLC, hay que asegurarse que es posible
  hacerlo.
- Se setean las ventanas deslizantes del emisor y receptor
- Se comienza el envío de X tramas a la otra estación
- A medida que van llegando se envían tramas de asentimiento.
- Cuando todas las tramas son asentidas se cierra la conexión y el envío
  termina.


## Casos

- Solo la estación A transmite
- Solo B transmite
- Ambas transmiten y asienten utilizando piggybacking


## A tener presente

- La simulación debe ser consistente con HDLC
- Formato de trama HDLC al enviar datos
- La solicitud de tramas supervisoras debe ser por medio del bit p/f
- El sistema debe simular la ocurrencia de errores dada una probabilidad
  (entrada de la aplicación)
- Las tramas se generan en el nivel de app 
- Hay que implementar rechazo simple o rechazo selectivo.


## Entrada

- Cantidad de tramas a enviar desde A y B //ok
- Tamaño de ventanas deslizantes (iguales para A y B) // ok
- Numero de tramas a enviar y recibir antes de recibir un ACK (hay que comprobar
  que no sea mayor que el tamaño de la ventana) //ok
- Probabilidad de producirse un error en el envío //ok


## Consideraciones

- Calidad de la simulación: Mostrar mensajes de lo que esta ocurriendo,
  identificar las tramas con su correspondiente número de secuencia, cuando
  ocurre un ACK o un rechazo, cuando se inicia una conexión, cuando se cierra.
