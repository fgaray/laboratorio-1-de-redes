#include <algorithm>
#include <set>
#include "Enlace.hpp"
#include "Mensaje.hpp"

#include "Retransmision.hpp"


void Enlace::initialize()
{
    //inicializamos las variables y constantes que vamos a usar a lo largo de la
    //ejecución del nivel de enlace

    this->direccion = par("direccion").longValue();
    this->tamano_ventana = par("tamanoVentana").longValue();
    this->tramas_antes_ack = par("tramasAntesAck").longValue();
    this->prob_error = par("probabilidadError").longValue();
    this->tiempo_retransmision = rand() % 100*5 + (this->tamano_ventana * 200 + 100); //tiempo de retransmisión random
    this->tiempo_envio_ack     = 200 * 5;                                               //se demora 200ms en ir y volver un paquete
    this->timer_ack = nullptr;

    //las ultimas tramas numeradas para las estaciones 
    this->ultima_trama_numerada1 = 0;
    this->ultima_trama_numerada2 = 0;
    this->ultima_trama_numerada3 = 0;
    this->ultima_trama_numerada4 = 0;

    this->numero_enviada = 0;

    this->trama_esperada1 
        = this->trama_esperada2 
        = this->trama_esperada3 
        = this->trama_esperada4 
        = 0;

    if(this->direccion == 1)
    {
        //la estación 1 es la encargada de crear el token 
        this->token = true;
        this->token_trama = new Token;
    }else{
        this->token = false;
    }





    this->iniciarVentana();
}


void Enlace::handleMessage(cMessage *m)
{

    cPacket *msg = static_cast<cPacket*>(m);

    if(msg->isSelfMessage() and not msg->arrivedOn("desde_fisico"))
    {
        this->handleMessage2(m, true, false);
    }else if(not msg->arrivedOn("desde_fisico")){
        this->handleMessage2(m, false, false);
    }else{


        Token *token = static_cast<Token*>(m);


        //debemos revisar si el token contiene algo


        if(token->getContiene())
        {
            //contiene algo, vemos si es para nosotros


            if(token->getDireccion() == this->direccion)
            {
                //es para nosotros, ponemos el token en 0 y lo retransmitimos

                this->handleMessage2(token->get(), false, true);

                //ahora volvemos a enviar el token pero con un valor de 0

                token->unsetToken();

                send(token, "hacia_fisico");

            }else if(token->getOrigen() == this->direccion){

                //nos llega de vuelta el token
                //Sacamos lo que sea que este en el token, esto quiere decir que
                //la trama llego ok

                if(token->llevaAck())
                {
                    //tenemos el token y era el que llevaba un ack asi que ahora
                    //podemos enviar un dato

                    token->unSetLlevaAck();

                    this->token_trama = token;
                    this->token = true;
                    this->enviar_pendientes();
                    
                }else{

                    TramaHDLC *trama = static_cast<TramaHDLC*>(token->get());
                    
                    if(token->getValor())
                    {
                        //volvió el token con valor 1 lo que nos indica que la
                        //estación receptora no leyó el contenido y por lo tanto
                        //debemos tratar esto como si fuera un ack negativo

                        handleAckNegativo(trama->getNumero());
                        token->sacarContenido();
                        token->unsetToken();

                    }else{

                        //sacamos el contenido del token y lo tratamos como si
                        //fuera un ack positivo
                        handleAckPositivo(trama->getNumero());
                        token->sacarContenido();

                    }

                    this->token = false;

                    send(token, "hacia_fisico");
                }

            }else{

                //no es para nosotros
                send(token, "hacia_fisico");
            }

        }else{

            //no contiene nada, es un token que podemos usar para enviar un
            //mensaje a otra estación

            this->token_trama = token;
            this->token = true;

            this->enviar_pendientes();
        }
    }
}


void Enlace::handleMessage2(cMessage *m, bool self, bool fisico)
{
    //Probamos de que tipo es la trama que nos llega
    
    cPacket *msg = static_cast<cPacket*>(m);

    TramaHDLC *t = dynamic_cast<TramaHDLC*>(m);

    Mensaje *mensaje_primero = dynamic_cast<Mensaje*>(m);

    if(not self and t != nullptr)
    {

        if(t->getAddress() != this->direccion and not this->token and t->getTipo() != Tipo::TramaU) 
        {
            //esta trama no es para nosotros, hay que reenviarla a la siguiente
            //trama

            cout << "Retransmitimos una trama con destino " << t->getAddress() << endl;

            send(m, "hacia_fisico");
            return;
        }

        if(t->getTipo() == Tipo::TramaU)
        {
            //nos llega el token

            if(this->inferior_ventana != this->superior_ventana)
            {
                //podemos enviar algo

                this->token = true;
                enviar_pendientes();
            }else{
                //reenviamos el token pero bajando en 1 la vida

                if(t->tieneVida())
                {
                    t->restarVida();
                    send(t, "hacia_fisico");
                }
            }
        }
    }



    //las tramas que pasan este punto son aquellas que llegan desde físico ya
    //sea porque tienen nuestra dirección o porque somos la estación con el
    //token




    if(self)
    {
        this->handleSelf(msg);
    }else if(fisico)
    {
        this->handleIncoming(msg);
    }else{
        //llega un mensaje de intermedio

        //vemos si podemos enviarlo, puede que la ventana no tenga espacio

        if(!this->podemos_enviar())
        {
            //no lo podemos enviar ahora :(, lo guardamos en un buffer hasta que
            //llegue un ack pero lo numeramos como trama

            stringstream s;

            s << "I, ";
            s << this->getUltimaTramaNumerada(msg);
            s << " desde " << this->direccion << " hacia " << mensaje_primero->getDestino();

            TramaHDLC *trama = new TramaHDLC(mensaje_primero->getDestino(), this->getUltimaTramaNumerada(msg), msg, s.str(), this->prob_error);
            this->aumentarUltimaTramaNumerada(msg);
            this->buffer_salida.push_back(trama);

        }else{
            //podemos enviar ahora mismo el paquete
            this->handleOutgoing(msg);
        }
    }
}



void Enlace::handleIncoming(cPacket *msg)
{
    TramaHDLC *trama = static_cast<TramaHDLC*>(msg);
    Mensaje *mensaje = static_cast<Mensaje*>(trama->getPayload());


    if(trama->conError() && trama->getTipo() == Tipo::TramaI && this->esTramaEsperada(trama))
    {
        //OK!, debemos checkear si llego ok esta trama, si es de información debemos
        //responder con un rechazo


        stringstream s;

        s << "REJ, ";
        s << trama->getNumero();
        s << " desde " << this->direccion << " hasta " << mensaje->getOrigen();


        TramaHDLC *ack = new TramaHDLC(mensaje->getOrigen()
                , trama->getNumero()
                , s.str()
                , this->prob_error);

        ack->setNack();
        ack->recalculateError();

        this->buffer_salida.insert(this->buffer_salida.begin(), ack);

        //vamos a ignorar todos los mensajes que lleguen que no sean el esperado
        this->cambiarTramaEsperada(trama);

        //ya que enviamos un rechazo, el receptor sabe que los envíos anteriores
        //a este esta ok
        this->tramas_por_enviar_ack.clear();


    }else if(trama->conError()){
        // llego con error, no es del tipo I así que simplemente no la tomamos
        // en cuenta
        return;
    }else{
        //llego ok la trama

        //es de información?
        if(trama->getTipo() == Tipo::TramaI)
        {

            //si no es el numero que esperábamos entonces lo ignoramos

            if(this->esTramaEsperada(trama))
            {

                Mensaje *mensaje = static_cast<Mensaje*>(trama->getPayload());

                this->tramas_por_enviar_ack.push_back(make_tuple(trama->getNumero(), mensaje->getOrigen()));

                //revisamos si debemos enviar un ack

                if(this->tramas_por_enviar_ack.size() > this->tramas_antes_ack)
                {
                    //enviamos un ack de vuelta a la estación que nos envió este
                    //paquete, pero solo si no podemos usar piggibacking

                    //ok, debemos enviar un ack de la ultima trama solo si ya no tenemos
                    //más datos en los cuales responder. Si hay datos entonces le
                    //corresponde enviar datos a la función que los esta enviando

                    this->responderAck();
                }else{
                    //ok, no enviamos un ack ahora...pero si pasa mucho tiempo
                    //hay que hacerlo para que el transmisor sepa que llego ok

                    this->setTimerAck(trama->getPayload());

                }

                //solo si era la trama esperada aumentamos a +1
                this->cambiarTramaEsperada(trama, 1);


                //pasamos la trama que nos llego al nivel superior
                cPacket *payload = trama->getPayload();
                send(payload, "hacia_arriba");
            }

            //revisamos si viene con piggybacking esta trama

            if(trama->contieneAck())
            {
                this->handleAck(trama->getAck());
            }

        }else if(trama->getTipo() == Tipo::TramaU){
            //puede que nos haya llegado el token


        }else{
            //es un ack, nos están respondiendo algo

            this->handleAck(trama->getAck());
        }
    }

}

void Enlace::handleAck(Ack ack)
{
    //depende de si el ack es positivo o no
    if(ack.getTipo() == TipoAck::Positivo)
    {

        handleAckPositivo(ack.getNumero());
    }else{
        handleAckNegativo(ack.getNumero());
    }
}


void Enlace::handleAckPositivo(int ack)
{
    //ok, llega el ack respondiendo palabras, primero debemos borrar todas las
    //tramas que sean de número menor a ack

    auto i = this->tramas_pendientes_respuesta.begin();

    //elementos que se van a borrar (i.e. los elementos que llegaron ok al
    //receptor y por lo tanto nos permiten aumentar la ventana)
    int elementos_borrar = 0;


    //ahora vemos que tramas llegaron ok
    for(; i != this->tramas_pendientes_respuesta.end(); ++i)
    {
        elementos_borrar++;
        if((*i)->getNumero() == ack - 1)
        {
            break;
        }
    }

    //borramos las tramas que llegaron ok al receptor
    //Borramos hasta i + 1 ya que erase es [first, last)

    if(i != this->tramas_pendientes_respuesta.end())
    {
        this->tramas_pendientes_respuesta.erase(this->tramas_pendientes_respuesta.begin(), i + 1);
    }else{
        this->tramas_pendientes_respuesta.erase(this->tramas_pendientes_respuesta.begin(), i);
    }


    //debemos ver si el ack nos permite enviar más tramas
    this->modificar_ventana(elementos_borrar);

    this->enviar_pendientes();
}


void Enlace::handleAckNegativo(int ack)
{
    //ok, llega el ack diciendo que "ack" esta malo y hay que volver a enviarlo


    vector<TramaHDLC*>::const_iterator i = tramas_pendientes_respuesta.begin();

    for(; i != tramas_pendientes_respuesta.end(); ++i)
    {
        if(ack == (*i)->getNumero())
        {
            //hay que empezar a enviar todo otra vez a partir de i :(
            break;
        }else{
            //las i - 1 tramas llegaron ok, así que hay que aumentar el tamaño de la
            //ventana para poder enviarlas
            this->superior_ventana++;
        }
    }

    if(i != tramas_pendientes_respuesta.end())
    {
        //debemos copiar las tramas pendientes de respuesta a al buffer de
        //salida a partir de i ya que i y las siguientes fueron ignoradas


        vector<TramaHDLC*>::const_iterator maximo = i;

        //copiar elementos a un vector auxiliar para ya que no podemos
        //simplemente poner elementos al inicio del vector "buffer_salida"

        vector<TramaHDLC*> aux;


        //copiamos los elementos del buffer de salida
        for(auto trama: this->buffer_salida)
        {
            aux.push_back(trama);
        }

        //borramos los elementos del buffer de salida
        this->buffer_salida.clear();

        //copiamos las tramas pendientes que llegaron mal al receptor y las
        //ponemos en la primera parte del buffer de salida, de esta forma van a
        //ser enviadas antes que el resto de las tramas que estaban por ser
        //enviadas
        for(; i != tramas_pendientes_respuesta.end(); ++i)
        {
            //cuando copiamos, aprovechamos de restaurar el tamano de la ventana
            //para poder enviar las tramas que llegaron mal

            this->inferior_ventana--;
            this->buffer_salida.push_back(*i);
        }

        //volvemos a copiar los elementos que se encontraban en el buffer de
        //salida
        this->buffer_salida.insert(this->buffer_salida.end(), aux.begin(), aux.end());

    }

    //borramos todo lo que tenemos pendiente de respuesta ya que o fue
    //respondido ok por el receptor o fue copiado nuevamente al buffer_salida
    this->tramas_pendientes_respuesta.clear();


    this->enviar_pendientes();
}


void Enlace::handleOutgoing(cPacket *msg)
{
    stringstream s;

    Mensaje *mensaje = static_cast<Mensaje*>(msg);

    //guardamos el paquete dentro de una trama HDLC
    s << "I, ";
    s << this->getUltimaTramaNumerada(msg);
    s << " desde " << this->direccion << " hasta " << mensaje->getDestino();

    TramaHDLC *trama = new TramaHDLC(mensaje->getDestino(), this->getUltimaTramaNumerada(msg), msg, s.str(), this->prob_error);
    this->aumentarUltimaTramaNumerada(msg);

    //enviamos la trama HDLC
    this->handleOutgoing(trama);

}


void Enlace::handleOutgoing(TramaHDLC *trama, bool no_retransmitir)
{

    if(not trama->contieneAck())
    {

        this->tramas_pendientes_respuesta.push_back(trama);
        this->inferior_ventana++;
        this->numero_enviada = trama->getNumero();

    }else{
        //si tiene ack entonces debemos ver que el token lleva un ack para no
        //considerarlo como envío de datos
        this->token_trama->setLlevaAck();
    }

    trama->recalculateError();

    if(!no_retransmitir and not trama->contieneAck())
    {
        scheduleAt(simTime() + this->tiempo_retransmision, new Retransmision(trama));
    }

    //hasta este punto ya deberíamos saber que tenemos un token ya que pasamos
    //la prueba

    this->token_trama->setOrigen(this->direccion);
    this->token_trama->setDestino(trama->getAddress());
    this->token_trama->setToken();
    this->token_trama->set(trama);

    this->token = false;


    send(token_trama, "hacia_fisico");


}

void Enlace::modificar_ventana(int mod)
{
    if(mod != 0)
    {
        if(mod > 0)
        {
            //hay que aumentar el tamaño de la ventana

            this->superior_ventana = this->superior_ventana + mod;

        }else{
            //hay que disminuir el tamaño de la ventana

            // - (-mod) = + mod
            this->inferior_ventana = this->inferior_ventana - mod;
        }
    }
}



bool Enlace::podemos_enviar()
{
    //hay que revisar si la ventana tiene espacio o si tenemos el token, en caso
    //contrario no podemos enviar


    if(not (this->inferior_ventana != this->superior_ventana))
    {
        //ok, no podemos seguir enviando asi que liberamos el token

        return false;
    }else{

        //finalmente si hay espacio en la ventana debemos ver si podemos enviar
        //solo si tenemos el token

        return this->token;
    }

}



void Enlace::handleOutgoingScheduled(size_t i, TramaHDLC *trama)
{
    cancelEvent(trama);
    scheduleAt(i/1000 + simTime(), trama);
}


void Enlace::enviar_pendientes()
{
    //vemos si hay algo que mandar
    if(not this->buffer_salida.empty())
    {
        size_t cont = 0;

        for(auto trama: this->buffer_salida)
        {
                this->handleOutgoingScheduled(cont, trama);
                cont++;
        }

        this->buffer_salida.clear();

    }
}


void Enlace::handleSelf(cPacket *msg)
{
    TramaHDLC *trama;

    bool retransmitir = true;

    Retransmision *ret = dynamic_cast<Retransmision*>(msg);
    TimeoutAck *to_ack = dynamic_cast<TimeoutAck*>(msg);

    if(to_ack != nullptr)
    {
        //hay que enviar un ack ya que no han llegado más paquetes

        this->responderAck();
        return;

    }else if(ret != nullptr){
        //gestionamos la retransmision de paquetes de los cuales no se han obtenido
        //acks
        trama = ret->getTrama();

        //hay que revisar si esta pendiente, no queremos volver a enviar una
        //trama que ya haya sido respondida como ok

        auto i = this->tramas_pendientes_respuesta.begin();

        for(; i != this->tramas_pendientes_respuesta.end(); ++i)
        {
            if((*i)->getNumero() == trama->getNumero())
            {
                break;
            }
        }

        //nos respondieron antes por lo tanto este timer es invalido
        if(this->tramas_pendientes_respuesta.end() == i)
        {
            return;
        }

        //borramos la trama de tramas pendientes ya que se va a volver a agregar
        //al ser enviada

        this->tramas_pendientes_respuesta.erase(i);

        //Retransmitimos
        //restauramos el valor de la ventana para poder enviar PERO solo si el
        //tamano de la ventana es menor que el que deberia ser
        if(this->inferior_ventana != trama->getNumero() && (this->superior_ventana - this->inferior_ventana) < this->tamano_ventana)
        {
            this->inferior_ventana = trama->getNumero();
        }else{
            return;
        }

        retransmitir = false;

    }else{
        trama = dynamic_cast<TramaHDLC*>(msg);

        if(trama == nullptr)
        {
            //es la trama para retransmitir la coneccion
            return;
        }
    }


    if(this->podemos_enviar())
    {
        handleOutgoing(trama, not retransmitir);
    }else{
        this->buffer_salida.push_back(trama);
    }
}


void Enlace::responderAck()
{
    //solo enviamos si hay algo que responder
    if(this->tramas_por_enviar_ack.size() >= 1)
    {
        //hay que buscar a que estaciones debemos responder


        //a quien debemos responder
        set<int> responder;


        for(auto t: tramas_por_enviar_ack)
        {
            int ntrama, origen;

            tie(ntrama, origen) = t;

            responder.insert(origen);
        }

        for(auto n: responder)
        {
            //para cada una de las estaciones que hay que responder debemos
            //generar un ack. Además debemos ver cual es la trama que debemos
            //responder 

            int numero = 0;

            for(auto t: tramas_por_enviar_ack)
            {
                int ntrama, origen;

                tie(ntrama, origen) = t;

                if(origen == n)
                {
                    numero = max(numero, ntrama);
                }
            }


            stringstream s;

            s << "RR, ";
            s << numero;
            s << " desde " << this->direccion << " hacia " << n; 

            // + 1 al ack ya que debemos responder con el siguiente ack
            TramaHDLC *ack = new TramaHDLC(n
                    , numero
                    , s.str()
                    , this->prob_error);

            ack->recalculateError();



            this->buffer_salida.insert(this->buffer_salida.begin(), ack);

        }

        this->tramas_por_enviar_ack.clear();
    }
}



void Enlace::setTimerAck(cPacket *paquete)
{

    Mensaje *mensaje = static_cast<Mensaje*>(paquete);

    TimeoutAck *to = new TimeoutAck(mensaje->getOrigen());

    if(this->timer_ack != nullptr)
    {
        //como llego un paquete nuevo hay que hacer un nuevo timer y sacamos el
        //viejo

        cancelEvent(this->timer_ack);
        delete this->timer_ack;
    }

    scheduleAt(this->tiempo_envio_ack + simTime(), to);
    this->timer_ack = to;
}




void Enlace::iniciarVentana()
{
    this->inferior_ventana = 0;
    this->superior_ventana = this->tamano_ventana;
}


bool Enlace::esTramaEsperada(TramaHDLC *trama)
{
    Mensaje *mensaje = static_cast<Mensaje*>(trama->getPayload());

    //dependiendo del origen es en que variable vamos a guardar la trama
    //esperada
    switch(mensaje->getOrigen())
    {
        case 1: {
            return this->trama_esperada1 == trama->getNumero();
            break;
        }

        case 2: {
            return this->trama_esperada2 == trama->getNumero();
            break;
        }

        case 3: {
            return this->trama_esperada3 == trama->getNumero();
            break;
        }

        case 4: {
            return this->trama_esperada4 == trama->getNumero();
            break;
        }

        default:
                cerr << "Llega una trama desde una estacion que no existe: " 
                    << mensaje->getOrigen() << endl;
                return false;
                break;

    }
}



void Enlace::cambiarTramaEsperada(TramaHDLC *trama, int mod)
{
    
    Mensaje *mensaje = static_cast<Mensaje*>(trama->getPayload());

    //dependiendo del destino es en que variable vamos a guardar la trama
    //esperada
    switch(mensaje->getOrigen())
    {
        case 1: 
            this->trama_esperada1 = trama->getNumero() + mod;
            break;

        case 2: 
            this->trama_esperada2 = trama->getNumero() + mod;
            break;

        case 3: 
            this->trama_esperada3 = trama->getNumero() + mod;
            break;

        case 4:
            this->trama_esperada4 = trama->getNumero() + mod;
            break;

        default:
            cerr << "Llega una trama desde una estacion que no existe: " 
                << mensaje->getOrigen() << endl;
            break;

    }
}


int Enlace::getUltimaTramaNumerada(cPacket *p)
{
    Mensaje *mensaje = static_cast<Mensaje*>(p);

    switch(mensaje->getDestino())
    {
        case 1:
            return this->ultima_trama_numerada1;
            break;
        case 2:
            return this->ultima_trama_numerada2;
            break;
        case 3:
            return this->ultima_trama_numerada3;
            break;
        case 4:
            return this->ultima_trama_numerada4;
            break;
    }

}



void Enlace::aumentarUltimaTramaNumerada(cPacket *p)
{
    Mensaje *mensaje = static_cast<Mensaje*>(p);

    switch(mensaje->getDestino())
    {
        case 1:
            this->ultima_trama_numerada1++;
            break;
        case 2:
            this->ultima_trama_numerada2++;
            break;
        case 3:
            this->ultima_trama_numerada3++;
            break;
        case 4:
            this->ultima_trama_numerada4++;
            break;
    }
}
