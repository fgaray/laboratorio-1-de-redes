#include "Retransmision.hpp"





Retransmision::Retransmision(TramaHDLC *trama): cPacket(("Retransmision: " + trama->toString()).c_str())
{
    this->trama = trama;
}


TramaHDLC *Retransmision::getTrama() const
{
    return this->trama;
}
