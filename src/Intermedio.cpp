#include "Intermedio.hpp"



void Intermedio::handleMessage(cMessage *msg)
{
    cPacket *paquete = static_cast<cPacket*>(msg);


    if(paquete->arrivedOn("desde_abajo"))
    {
        //el mensaje llegó desde abajo (nivel de acceso al medio) por lo que
        //debemos pasarlo al nivel de aplicación (nivel superior)
        send(msg, "hacia_arriba");
    }else{

        //el mensaje llegó desde aplicación así que hay que enviarlo al nivel de
        //acceso al medio
        send(msg, "hacia_abajo");
    }
}
