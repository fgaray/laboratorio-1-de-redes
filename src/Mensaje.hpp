#ifndef _H_MENSAJE_
#define _H_MENSAJE_

#include <omnetpp.h>


#include <string>
using namespace std;


/**Clase para enviar mensajes desde el nivel de aplicación ya que debemos saber
 * a que estación quiere enviar mensajes.
 * */
class Mensaje: public cPacket{

    private:
        /**El destino y origen del mensaje*/
        int destino, origen;

        /**El mensaje contenido*/
        string mensaje;

    public:

        /**Constructor de la clase
         *
         * @param destino El destino del mensaje
         * @param origen El origen del mensaje
         * @param mensaje El mensaje que se quiere enviar a otras estaciones
         * @param label El nombre que se quiere mostrar en la aplicación
         *
         * */
        Mensaje(int destino, int origen, string mensaje, string label);


        /*Devuelve la dirección de la estación destino del mensaje**/
        int getDestino() const;

        /*Devuelve la dirección de la estación de origen del mensaje**/
        int getOrigen() const;
};


#endif /* end of include guard: _H_MENSAJE_ */
