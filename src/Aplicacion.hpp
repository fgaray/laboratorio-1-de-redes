#include <vector>
#include <omnetpp.h>

using namespace std;



/**Define el módulo de nivel de aplicación
 *
 * Este modulo envía y recibe mensajes de otras estaciones. Crea mensajes sin
 * contenido que son enviadas a través del nivel intermedio*/
class Aplicacion: public cSimpleModule{

    private:

        //los datos recibidos
        vector<cMessage*> datos;

        //el numero de tramas a enviar
        long numero_tramas;

        //la dirección de esta estación
        int direccion;


    public:

        /**Esta función es llamada al inicio de la simulación*/
        virtual void initialize();

        /**Se llama a esta función cada vez que este modulo recibe un mensaje
         * */
        virtual void handleMessage(cMessage *msg);



        /**Función a ser llamada cuando la simulación termine
         * */
        virtual void finish();

};


Define_Module(Aplicacion);
