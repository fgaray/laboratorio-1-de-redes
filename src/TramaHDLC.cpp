#include "TramaHDLC.hpp"

#include <cstdlib>
#include <sstream>



TramaHDLC::TramaHDLC(int address, int numero, Tipo tipo, const string &nombre, int prob_error): cPacket(nombre.c_str())
{
    this->pf = false;
    this->numero = numero;
    this->address = address;
    this->contiene_ack = false;
    this->tipo = tipo;
    this->prob_error = prob_error;
    this->payload = nullptr;
    this->vida = 8;

    srand(time(NULL));

    this->recalculateError();
}


TramaHDLC::TramaHDLC(int address, int ack, const string &nombre, int prob_error): TramaHDLC(address, 0, Tipo::TramaS, nombre, prob_error)
{
    this->ack = Ack(ack, TipoAck::Positivo);
    this->contiene_ack = true;
}


TramaHDLC::TramaHDLC(int address, int numero, cPacket *payload, const string &nombre, int prob_error): TramaHDLC(address, numero, Tipo::TramaI, nombre, prob_error)
{
    this->payload = payload;
}


TramaHDLC::TramaHDLC(int address, Command comando, const string &nombre, int prob_error): TramaHDLC(address, 0, Tipo::TramaU, nombre, prob_error)
{
    this->comando = comando;
    this->setKind(4);
}


void TramaHDLC::setPf()
{
    this->pf = true;
}


bool TramaHDLC::getPf() const
{
    return this->pf;
}

bool TramaHDLC::conError() const
{
    return this->error;
}


int TramaHDLC::getAddress() const
{
    return this->address;
}

Tipo TramaHDLC::getTipo() const
{
    return this->tipo;
}


int TramaHDLC::getNumero() const
{
    return this->numero;
}

void TramaHDLC::setAck(int numero)
{
    this->ack = Ack(numero, TipoAck::Positivo);
    this->contiene_ack = true;
}

void TramaHDLC::setNack(int numero)
{
    this->ack = Ack(numero, TipoAck::Negativo);
    this->contiene_ack = true;
}

Ack TramaHDLC::getAck() const
{
    return this->ack;
}


bool TramaHDLC::contieneAck() const
{
    return this->contiene_ack;
}

string TramaHDLC::toString() const
{
    stringstream s;
    s << "Trama HDLC: ";

    switch(this->tipo)
    {
        case Tipo::TramaI:
            s << "Trama de información ";
            break;
        case Tipo::TramaS:
            s << "Trama supervisora ";
            break;
        default:
            s << "Sin tipo! ";
            break;
    }


    s << "Numero: ";
    s << this->numero;
    s << (this->error ? ", con error, " : ", sin error, ");


    if(this->contiene_ack)
    {
        if(this->tipo == Tipo::TramaI)
        {
            s << "Piggybacking: ";
            s << this->ack.toString();
        }else{
            s << "Ack: ";
            s << this->ack.toString();
        }
    }

    return s.str();
}


const char *TramaHDLC::toCString() const
{
    return (this->toString()).c_str();
}


void TramaHDLC::setNack()
{
    this->ack = Ack(this->ack.getNumero(), TipoAck::Negativo);
}

void TramaHDLC::recalculateError()
{
    //solo las de informacion pueden llegar mal
    if(this->tipo == Tipo::TramaI)
    {
        if(tipo == Tipo::TramaI)
        {
            //la probabilidad de que haya un error es un rango de 0 a 100.
            if(rand() % 100 < this->prob_error)
            {
                this->error = true;
            }else{
                this->error = false;
            }
        }else{
            this->error = false;
        }


    }else{
        this->error = false;
    }


    if(this->error and this->tipo == Tipo::TramaI)
    {
        this->setKind(0);
    }else if(this->tipo == Tipo::TramaI){
        this->setKind(1);
    }

    if(this->tipo == Tipo::TramaS)
    {
        this->setKind(2);
    }

}


cPacket *TramaHDLC::getPayload() const
{
    return this->payload;
}


Command TramaHDLC::getCommand() const
{
    return this->comando;
}



bool TramaHDLC::tieneVida() const
{
    return this->vida > 0;
}

void TramaHDLC::restarVida()
{
    this->vida--;
}



