#ifndef _H_RETRANSMISION_
#define _H_RETRANSMISION_

#include <omnetpp.h>

#include "TramaHDLC.hpp"

class Retransmision: public cPacket{

    private:
        TramaHDLC *trama;

    public:
        Retransmision(TramaHDLC *trama);

        TramaHDLC *getTrama() const;
};


#endif /* end of include guard: _H_RETRANSMISION_ */
