#include "Aplicacion.hpp"

#include <sstream>

#include "Mensaje.hpp"




void Aplicacion::initialize()
{
    //inicializamos los valores

    this->numero_tramas = par("numeroTramas").longValue();
    this->direccion = par("direccion").longValue();


    //enviamos numero_tramas tramas

    for(unsigned int estacion = 1; estacion <= 4; estacion++)
    {
        //nos aseguramos que no nos enviamos mensajes a nosotros 
        if(estacion != direccion)
        {
            for(long i = 0; i < this->numero_tramas; i++)
            {
                stringstream s;

                s << "Mensaje ";
                s << i;

                Mensaje *msg = new Mensaje(estacion, direccion, "Mensaje :)", s.str().c_str());

                //los mensajes van a ser de color negro
                msg->setKind(7);


                //enviamos hacia intermedio el paquete
                send(msg, "hacia_abajo");
            }
        }
    }
}


void Aplicacion::handleMessage(cMessage *msg)
{
    //cada mensaje que recibimos lo guardamos

    datos.push_back(msg);
}



void Aplicacion::finish()
{

    //al terminar la simulación mostramos cuantos mensajes nos llegaron como
    //estación

    stringstream s;

    s << "Soy " << this->direccion;
    s << " y me llegaron ";
    s << this->datos.size();
    s << " mensajes";

    bubble(s.str().c_str());

    cout << s.str() << endl;
}
