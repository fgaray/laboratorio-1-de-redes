#include "Mensaje.hpp"



Mensaje::Mensaje(int destino, int origen, string mensaje, string label): cPacket(label.c_str())
{
    this->mensaje = mensaje;
    this->destino = destino;
    this->origen = origen;
}



int Mensaje::getDestino() const
{
    return this->destino;
}


int Mensaje::getOrigen() const
{
    return this->origen;
}
