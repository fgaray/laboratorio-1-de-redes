#include "Token.hpp"


Token::Token()
{
    this->contiene = false;
    this->valor = false;
    this->llevo_ack = false;
}



void Token::set(cPacket *trama)
{
    this->trama = trama;
    this->contiene = true;
    this->setKind(trama->getKind());
}



cPacket *Token::get()
{
    return this->trama;
}



bool Token::getContiene() const
{
    return this->contiene;
}




bool Token::getValor() const
{
    return this->valor;
}


int Token::getDireccion() const
{
    return this->destino;
}


void Token::unsetToken()
{
    this->setKind(3);
    this->valor = false;
}


void Token::setToken()
{
    this->valor = true;
}

int Token::getOrigen()
{
    return this->origen;
}

void Token::setOrigen(int origen)
{
    this->origen = origen;
}


void Token::setDestino(int destino)
{
    this->destino = destino;
}


void Token::sacarContenido()
{
    this->setKind(4);
    this->contiene = false;
}



bool Token::llevaAck() const
{
    return this->llevo_ack;
}

void Token::setLlevaAck()
{
    this->llevo_ack = true;
}

void Token::unSetLlevaAck()
{
    this->llevo_ack = false;
}


