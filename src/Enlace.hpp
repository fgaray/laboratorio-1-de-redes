#include <omnetpp.h>
#include <vector>
#include <tuple>


using namespace std;

#include "TramaHDLC.hpp"
#include "Ack.hpp"
#include "TimeoutAck.hpp"
#include "TODesconectar.hpp"
#include "Token.hpp"



/**Implementa token ring mezclado con HDLC en el nivel de enlace*/
class Enlace: public cSimpleModule{

    private:
        vector<TramaHDLC*> buffer_salida; //se guardan las que no pueden ser enviadas por la ventana
        vector<TramaHDLC*> tramas_pendientes_respuesta; //las tramas que estamos esperando a que nos digan si llegaron ok
        vector<tuple<int, int>> tramas_por_enviar_ack; //las tramas que hemos recibido y que aun no enviamos acks. (NTrama, Origen Trama)

        vector<cPacket*>::const_iterator trama_por_enviar;

        //la ultima trama que fue numerada en este enlace
        //Se mantiene un contador para cada una de las estaciones ya que a la
        //estación que le llegue la trama numerada va a esperar que de la
        //estación X le lleguen tramas que partan desde el 0
        int ultima_trama_numerada1,
            ultima_trama_numerada2,
            ultima_trama_numerada3,
            ultima_trama_numerada4;


        //el tiempo que vamos a esperar para las retransmisiones
        int tiempo_retransmision;

        //tiempo que vamos a esperara que llegue un paquete antes de enviar aun
        //ack aunque no tengamos suficientes paquetes para enviar
        int tiempo_envio_ack;


        //Timer para enviar acks a la otra estación si no hemos recibido nada
        TimeoutAck *timer_ack;

        //el número e la ultima trama enviada
        int numero_enviada;


        //la dirección de esta estación
        int direccion;

        //este booleano nos indica si tenemos el token o no
        bool token;

        //Un puntero al token
        Token *token_trama;



        /**Revisa los mensajes que llegan desde físico
         *
         * A partir del tipo de paquete que llegue ejecuta una acción en
         * particular
         * */
        void handleIncoming(cPacket *msg);


        /**Envía el paquete a físico dado a físico
         *
         * @param msg El paquete a enviar a físico
         *
         * */
        void handleOutgoing(cPacket *msg);



        /**Envía el paquete dado a físico
         *
         * @param msg La trama a enviar a físico
         *
         * @param no_retransmitir Si no hay que retransmitir la trama
         *
         * */
        void handleOutgoing(TramaHDLC *msg, bool no_retransmitir = false);




        /**Envía un mensaje a la misma estación para que sea procesado en el
         * futuro
         *
         * @param i El número de mensaje que se quiere enviar (1, 2, 3)
         * @param trama La trama que se quiere recibir en el futuro
         *
         *
         * */
        void handleOutgoingScheduled(size_t i, TramaHDLC *trama);


        //el tamaño de la ventana, las tramas a recibir antes de enviar acks y la
        //probabilidad de error de las tramas
        int tamano_ventana, tramas_antes_ack, prob_error;

        //la trama que se espera que llegue desde físico
        //Debemos tener un contador por cada estación ya que la trama que
        //esperamos que llegue desde cada una de las estaciones es distinta y
        //depende de la estación
        int trama_esperada1, trama_esperada2, trama_esperada3, trama_esperada4;


        //El valor de la ventana para enviar datos. Con esto podemos limitar la
        //cantidad de paquetes que podemos enviar en una sesión
        int inferior_ventana, superior_ventana;

        /**A partir del ack que llega, copia las tramas con el número igual a
         * ack o mayor que ack al inicio del vector buffer_salida para que la
         * próxima trama que se tenga que enviar sea una trama que llego de
         * forma incorrecta al receptor
         *
         * @param ack El número de trama que llego mal al receptor
         *
         * */
        void handleAckNegativo(int ack);

        /**A partir del numero de ack que se recibe liberamos un espacio en la
         * ventana (vemos cuantos hay que liberar) y tambien se borran los acks
         * correspondiente en buffer_salida
         *
         * @param El numero de trama que quiere el receptor que le enviemos a
         * continuación
         * */
        void handleAckPositivo(int ack);


        /**Interpreta el ack ya sea un ack positivo o negativo y retransmite los
         * mensajes si es necesario además de mover la ventana
         *
         * @param ack El ack a ser interpretado
         *
         * */
        void handleAck(Ack ack);


        /**A partir del numero dado modifica la ventana agregando espacios o
         * borrando espacios
         *
         * @param mod Un número positivo o negativo que nos indica como se debe
         * modificar la ventana. Un número positivo nos indica que hay que
         * aumentar la ventana en mod posiciones, uno negativo que hay que
         * disminuirla en mod posiciones
         * */
        void modificar_ventana(int mod);


        /**Verifica si podemos enviar o no una trama en base a la ventana
         *
         * @return true si podemos enviar, false si no podemos enviar una nueva
         * trama*/
        bool podemos_enviar();


        /**Envía las tramas que están pendientes (guardadas en el
         * buffer_salida). Si no es posible enviarlas (por la ventana), entonces
         * se envían como un mensaje a si mismos para ser gestionados después
         * */
        void enviar_pendientes();


        /**Recibe un paquete que fue enviado a nosotros mismos y dependiendo del
         * tipo de paquete  lo envía por físico (trama pendiente), responde un
         * ack (timeout) o retransmitimos un paquete
         *
         * @param msg Un paquete que nos enviamos a notros
         *
         * */
        void handleSelf(cPacket *msg);


        /**Responde con un ack a la estación que nos envió tramas
         *
         * */
        void responderAck();

        /**Inicia un nuevo timer para responder con acks cuando no lleguen más
         * tramas y no tengamos suficientes tramas para poder responder (por el
         * limite de tramas antes de acks)
         *
         * */
        void setTimerAck(cPacket *paquete);


        /**Inicia la ventana. Se llama una vez que tengamos lista la conexión
         * así cuando nos lleguen paquetes y no tengamos una conexión podemos
         * reutilizar el mismo código que tenemos para guardar las tramas cuando
         * no hay espacio en la ventana
         * */
        void iniciarVentana();



        /**Libera el token y lo envía por la red para que otra estación pueda
         * enviar paquetes
         * */
        void enviarToken();


        /**Indica si la trama dada es la trama que se esperaba que llegara a la
         * estación (si corresponde su número)
         *
         * @param trama La trama que se quiere verificar si es la que se
         * esperaba
         *
         * @return true si es la trama que se esperaba, false en caso contrario
         * */
        bool esTramaEsperada(TramaHDLC *trama);


        /**Actualiza los valores de las tramas esperadas para la estación que
         * nos envió la trama dada. Por defecto cambia el valor de la trama
         * esperada al número de la trama dada pero se puede sumar o restar ese
         * número por medio del modificador
         *
         * @param trama La trama con la cual se quiere modificar el valor de la
         * última trama esperada
         *
         * @param mod El modificador que se quiere utilizar para cambiar la
         * última trama esperada. Por ejemplo puede ser mod = 1 si es que la
         * trama es correcta y se quiere que la siguiente trama sea la correcta
         *
         * */
        void cambiarTramaEsperada(TramaHDLC *trama, int mod = 0);




        /**Devuelve el valor de la última trama numerada que se utilizó para
         * enviar un paquete de una estación en particular (la de destino del
         * paquete p)
         *
         * @param p El paquete que se quiere numerar
         *
         * @return El número de la última trama que se numeró para el destino
         * del paquete p
         *
         * */
        int getUltimaTramaNumerada(cPacket *p);


        /**Actualizamos el valor de la última trama numerada de acuerdo a la
         * dirección de destino del paquete p
         *
         * @param p El paquete que se va a utilizar para aumentar la ultima
         * trama numerada de la estación de destino del paquete p
         *
         * */
        void aumentarUltimaTramaNumerada(cPacket *p);

        /**Esta función es la que maneja los paquetes que van dentro del token
         * */
        void handleMessage2(cMessage *m, bool self, bool fisico);

    public:
        void initialize();


        /**Esta función recibe los paquetes entrantes y los administra según el
         * token*/
        virtual void handleMessage(cMessage *m);

};


Define_Module(Enlace);

