#ifndef _H_ACK_
#define _H_ACK_

#include <string>
using namespace std;


/**El tipo de ack, puede ser un ack positivo o negativo*/
enum class TipoAck{
  Positivo,
  Negativo
};



/**Representa un ack con el nunero que esta respondiendo o rechazando*/
class Ack{

  private:
    TipoAck tipo;
    int numero;

  public:
    Ack(int numero, TipoAck tipo);
    Ack();

    /**Devuelve el tipo del ack
     * */
    TipoAck getTipo() const;

    /**Devuelve el número del ack
     * */
    int getNumero() const;

    /**Devuelve un string que representa al ack*/
    string toString() const;
};



#endif /* end of include guard: _H_ACK_ */
