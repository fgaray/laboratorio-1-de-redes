#include "Ack.hpp"

#include <sstream>



Ack::Ack(int numero, TipoAck tipo)
{
    this->numero = numero;
    this->tipo = tipo;
}

Ack::Ack()
{

}

TipoAck Ack::getTipo() const
{
    return this->tipo;
}


int Ack::getNumero() const
{
    return this->numero;
}

string Ack::toString() const
{
    stringstream s;

    switch(this->tipo)
    {
        case TipoAck::Positivo:
            s << "positivo (";
            break;
        case TipoAck::Negativo:
            s << "negativo (";
            break;
    }

    s << this->numero << ") ";
    

    return s.str();
}
