#ifndef _H_TOKEN_
#define _H_TOKEN_

#include <omnetpp.h>



/**Esta clase representa el token que va dando vueltas por el anillo*/
class Token: public cPacket{

    private:
        /**
         * valor Es el valor que tiene el token, puede ser 0 o 1
         * contiene: Si el token contiene una trama en su interior
         * llevo_ack: Si el token esta llevando un ack o no
         * */
        bool valor, contiene, llevo_ack;

        // la trama que lleva el token
        cPacket *trama;

        //el destino y origen del token
        int destino, origen;

    public:
        /**constructor*/
        Token();


        /**Agrega una trama al token
         *
         * @param trama La trama a agregar al token
         * */
        void set(cPacket *trama);

        /**Obtiene el paquete que va dentro del token
         *
         * @return El paquete que va dentro del token
         * */
        cPacket *get();


        /**Indica si contiene un paquete dentro del token
         *
         * @return true si hay un paquete en el token, false en caso contrario
         *
         * */
        bool getContiene() const;


        /**Remueve el contenido del token
         * */
        void sacarContenido();

        /**Devuelve el valor del token
         *
         * @return true si el token es 1, false si el token es 0
         *
         * */
        bool getValor() const;

        /**Devuelve la dirección de destino del token
         *
         * @return La dirección de destino
         *
         * */
        int getDireccion() const;

        /**Cambia el valor del token a 0
         * */
        void unsetToken();


        /**Cambia el valor del token a 1*/
        void setToken();

        /**Devuelve el origen del token
         *
         * @return La dirección de origen del token
         *
         * */
        int getOrigen();


        /**Cambia el origen del token 
         *
         * @param origen La nueva dirección de origen del token
         *
         * */
        void setOrigen(int origen);


        /**Cambia la dirección de destino del token
         *
         * @param destino La nueva dirección de origen del token
         *
         * */
        void setDestino(int destino);


        /**Indica si el token lleva o no un ack
         *
         * @return true si lleva un ack, false en caso contrario
         *
         * */
        bool llevaAck() const;


        /**Cambia el valor de si el token lleva o no un ack a verdadero
         * */
        void setLlevaAck();

        /**Cambia el valor de si el token lleva o no un ack a falso
         * */
        void unSetLlevaAck();
};


#endif /* end of include guard: _H_TOKEN_ */
