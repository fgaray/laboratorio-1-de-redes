#include "TimeoutAck.hpp"



TimeoutAck::TimeoutAck(int destino): cPacket("Retransmision ack")
{
    this->destino = destino;
}



int TimeoutAck::getDestino() const
{
    return this->destino;
}
