#include <omnetpp.h>



/**Este modulo solo repite los mensajes, no hace nada por ahora, solo actúa como
 * un puente entre el nivel de aplicación y el nivel de acceso al medio*/
class Intermedio: public cSimpleModule{

    private:

    public:

        /**Esta función va a recibir todos los mensajes que le lleguen a la
         * estación, dependiendo de si es un mensaje desde aplicación o
         * intermedio va a reenviar el mensaje hacia un nivel superior o
         * inferior
         * */
        virtual void handleMessage(cMessage *msg);
};


Define_Module(Intermedio);
