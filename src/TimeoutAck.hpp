#ifndef _H_TIMEOUTACK_
#define _H_TIMEOUTACK_

#include <omnetpp.h>


class TimeoutAck: public cPacket{

    private:
        int destino;

    public:
        TimeoutAck(int destino);


        int getDestino() const;

};


#endif /* end of include guard: _H_TIMEOUTACK_ */
