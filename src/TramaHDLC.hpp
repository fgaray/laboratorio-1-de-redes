#ifndef _H_TRAMAHDLC_
#define _H_TRAMAHDLC_

#include <omnetpp.h>
#include <vector>
#include <string>


#include "Ack.hpp"

using namespace std;



/**Los tipos de tramas que pueden ser enviadas
 * */
enum class Tipo{
    TramaI,         // transportan data y pueden llevar acks por piggibacking
    TramaS,         // no transportan datos. Solo responden acks
    TramaU
};


/**Los comandos que se pueden enviar en las tramas no numeradas
 * */
enum class Command{

    SABM,            //Set Async Balanced 3 Mode
    SABME,           //Set ABM Extended Mode
    DISC,            //disconnect

    Nothing         // es simplemente un trama UA
};



/**Este modulo solo repite los mensajes, no hace nada por ahora*/
class TramaHDLC: public cPacket{

    private:
        /**Control*/

        int vida;

        //Si lleva el bit pf activado
        bool pf;

        //el numero de trama
        int numero;

        //si la trama tiene  error
        bool error;

        //la direccion de destino de la trama
        int address;

        //El tipo de la trama. Puede ser de informacion, supervisora o no
        //numerada
        Tipo tipo;

        //Los datos transportados por la trama
        cPacket *payload;

        
        //indica si contiene un ack
        bool contiene_ack;

        //El ack que lleva la trama
        Ack ack;

        //la probabilidad de error de que esta trama no llegue al destino
        int prob_error;


        //El comando que lleva esta trama si es una no numerada
        Command comando;


    public:

        /**Constructores
         *
         * @param address La direccion de destino de la trama
         *
         * @param numero El numero de trama
         *
         * @param nombre El nombre a mostrar cuando se vea en el simulador
         *
         * @param prob_error La probabilidad de error de esta trama
         * */
        TramaHDLC(int address, int numero, Tipo tipo, const string &nombre, int prob_error);
        TramaHDLC(int address, int numero, cPacket *payload, const string &nombre, int prob_error);


        /** Contructor para las tramas que envian acks. Estas tramas no pueden
         * llevar un payload y solo responden acks. Por defecto son tramas de
         * tipo supervisoras
         *
         * @param address La direccion de destino de la trama
         *
         * @param ack El ack que va a responder esta trama
         *
         * @param nombre El nombre a mostrar cuando se vea en el simulador
         *
         * @param prob_error La probabilidad de error de esta trama
         * */
        TramaHDLC(int address, int ack, const string &nombre, int prob_error);


        /** Constructor para tramas no numeradas
         *
         * @param address La direccion de destino de la trama
         *
         * @param comando El comando que se va a enviar en las tramas
         *
         * @param nombre El nombre a mostrar cuando se vea en el simulador
         *
         * @param prob_error La probabilidad de error de esta trama
         * */
        TramaHDLC(int address, Command comando, const string &nombre, int prob_error);



        /**Setea a 1 el bit pf de esta trama
         * */
        void setPf();

        /**Obtiene si el bit p/f esta activo
         *
         * @return true si el bit p/f esta activo, false en caso contrario
         * */
        bool getPf() const;


        /**Devuelve la direccion de destino de la trama
         * */
        int getAddress() const;


        /**Indica si la trama contiene un error
         *
         * @return true si la trama tiene un error, false en caso contrario
         * */
        bool conError() const;

        /**Devuelve el tipo de trama
         *
         * @return El tipo de trama. Puede ser de informacion, supervisora o no
         * numerada.
         * */
        Tipo getTipo() const;

        /**Devuelve el numero de trama
         * */
        int getNumero() const;

        /**Agrega un ack (RR) a la trama. Util para hacer piggibacking
         *
         * @param numero El numero que vamos a responder afirmativamente
         * */
        void setAck(int numero);

        /**Agrega un NACK a la trama (REJ). Para hacer piggibacking
         *
         * @param numero El numero de trama que vamos a rechazar
         * */
        void setNack(int numero);

        /**Pone la trama actual como un rechazo*/
        void setNack();

        /**Devuelve el ack contenido en la trama*/
        Ack getAck() const;



        /**Indica si contiene un ack la trama
         *
         * @return Devuelve true si existe un ack en la trama, false en caso
         * contrario
         * */
        bool contieneAck() const;


        /**Devuelve una representacion en string de la trama*/
        string toString() const;
        const char *toCString() const;

        /**Calcula si esta trama va con error o no*/
        void recalculateError();


        /**Devuelve el contenido de la trama*/
        cPacket *getPayload() const;

        /**Devuelve el comando transportado por la trama*/
        Command getCommand() const;


        bool tieneVida() const;

        
        void restarVida();








};

#endif /* end of include guard: _H_TRAMAHDLC_ */
